﻿namespace PhonewordX
{
    public interface IDialer
    {
        bool Dial(string number);
    }
}
